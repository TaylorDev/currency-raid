package incode.air.cryptoraid.data.remote

object RemoteContract {

    //Urls
    const val BASE_API_LAYER = "https://min-api.cryptocompare.com/data/"
    const val ALL_DATA = "all/coinlist"
    const val PRICE_DATA = "pricemulti"
    const val HISTORICAL_DAILY = "histoday"
    const val HISTORICAL_HOURLY = "histohour"

    //Query
    const val FSYMS = "fsyms"
    const val FSYM = "fsym"
    const val TSYMS = "tsyms"
    const val TSYM = "tsym"
    const val LIMIT = "limit"


}