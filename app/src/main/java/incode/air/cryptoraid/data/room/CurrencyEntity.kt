package incode.air.cryptoraid.data.room

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = RoomContract.TABLE_CURRENCIES)
data class CurrencyEntity(
        @PrimaryKey val id: Long,
        val currencyName: String,
        val currencySymbol: String,
        val currencyImageUrl:String,
        val currencyPrice: String,
        val percentChange24h: String

)