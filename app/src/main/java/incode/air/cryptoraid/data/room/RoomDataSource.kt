package incode.air.cryptoraid.data.room

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(entities = [(CurrencyEntity::class)],
        version = 1)
abstract class RoomDataSource: RoomDatabase() {

    abstract fun currencyDao(): RoomCurrencyDao

}

fun buildPersistentCurrency(context: Context): RoomDataSource = Room.databaseBuilder(
        context.applicationContext,
        RoomDataSource::class.java,
        RoomContract.DATABASE_CURRENCY
).build()