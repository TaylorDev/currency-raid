package incode.air.cryptoraid.data.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.pixplicity.easyprefs.library.Prefs
import incode.air.cryptoraid.model.CurrencyModel
import incode.air.cryptoraid.data.remote.RemoteDataSource
import incode.air.cryptoraid.data.room.RoomDataSource
import incode.air.cryptoraid.model.HistoricalModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class Repository@Inject constructor(
        private val roomDataSource: RoomDataSource,
        private val remoteDataSource: RemoteDataSource) : RepositoryApp {

    val allCompositeDisposable: MutableList<Disposable> = arrayListOf()

    override fun getAllCurrencyData(): LiveData<MutableList<CurrencyModel>> {
        val mutableLiveData = MutableLiveData<MutableList<CurrencyModel>>()
        val disposable = remoteDataSource.requestAllCurrencyList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ currencyResponse ->
                    mutableLiveData.value = transform(currencyResponse)
                }, {
                    t: Throwable -> Log.e("Error", "getAllCurrencyData()", t)
                })
        allCompositeDisposable.add(disposable)
        return mutableLiveData
    }

    override fun getAllPriceData(liveData: MutableList<CurrencyModel>): LiveData<MutableList<CurrencyModel>> {
        val mutableLiveData = MutableLiveData<MutableList<CurrencyModel>>()
        val disposable = remoteDataSource.requestAllPriceData(getListCurrency(liveData), getCurrencyExchange(Prefs.getInt("currency", 0)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ currencyResponse ->
                    mutableLiveData.value = transform(currencyResponse.body()?.string(), liveData)
                }, {
                    t: Throwable -> Log.e("Error", "getAllCurrencyData()", t)
                })
        allCompositeDisposable.add(disposable)
        return mutableLiveData
    }

    override fun getHistoricalDaylyData(fsym: String, tsym: String, limit: Int): LiveData<HashMap<String, HistoricalModel>> {
        val mutableLiveData = MutableLiveData<HashMap<String, HistoricalModel>>()
        val disposable = remoteDataSource.requestHistoricalDaily(fsym, tsym, limit)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ historicalData ->
                    mutableLiveData.value = transformHistorical(historicalData)
                }, {
                    t: Throwable -> Log.e("Error", "getAllCurrencyData()", t)
                })
        allCompositeDisposable.add(disposable)
        return mutableLiveData
    }

    override fun getHistoricalHorlyData(fsym: String, tsym: String, limit: Int): LiveData<HashMap<String, HistoricalModel>> {
        val mutableLiveData = MutableLiveData<HashMap<String, HistoricalModel>>()
        val disposable = remoteDataSource.requestHistoricalHourly(fsym, tsym, limit)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ historicalData ->
                    mutableLiveData.value = transformHistorical(historicalData)
                }, {
                    t: Throwable -> Log.e("Error", "getAllCurrencyData()", t)
                })
        allCompositeDisposable.add(disposable)
        return mutableLiveData
    }

    private fun getListCurrency(listData: MutableList<CurrencyModel>): String {
        var dataCurrency = ""
        listData.forEach({
            when {
                dataCurrency != "" -> dataCurrency += "," + it.symbol.toUpperCase()
                else -> dataCurrency = it.symbol.toUpperCase()
            }
        })
        return dataCurrency
    }

    private fun transformHistorical(historicalData: Response<ResponseBody>?): HashMap<String, HistoricalModel>? {

        val jsonData = JSONObject(historicalData?.body()?.string())

        val array = jsonData.getJSONArray("Data")

        val historicalData: HashMap<String, HistoricalModel> = HashMap()
        for (i in 0 until array.length()) {
            historicalData[array.getJSONObject(i).getString("time")] = HistoricalModel(array.getJSONObject(i).getDouble("close"),
                    array.getJSONObject(i).getDouble("high"),
                    array.getJSONObject(i).getDouble("low"),
                    array.getJSONObject(i).getDouble("open"),
                    array.getJSONObject(i).getDouble("volumefrom"),
                    array.getJSONObject(i).getDouble("volumeto"))
        }
        return historicalData
    }

    private fun transform(data: String?, liveData: MutableList<CurrencyModel>): MutableList<CurrencyModel>? {
        try {
            val jsonObj = JSONObject(data)
            val iterator = jsonObj.keys()
            while (iterator.hasNext()) {
                val key = iterator.next()
                liveData.forEach {
                    val value = jsonObj.getJSONObject(key)
                    if (it.symbol == key) {
                        it.priceData = value.getString(getCurrencyExchange(Prefs.getInt("currency", 0)))
                    }
                }
            }
            return liveData
        }catch (ex: Exception){
            return null
        }

    }

    private fun transform(data: retrofit2.Response<ResponseBody>): MutableList<CurrencyModel>? {
        try {
            val jsonObj = JSONObject(data.body()?.string())
            val array = jsonObj.getJSONObject("Data")
            val iterator = array.keys()
            val currencyArray: MutableList<CurrencyModel> = ArrayList()
            while (iterator.hasNext()) {
                    val key = iterator.next()
                    val value = array.getJSONObject(key)
                    try {
                        currencyArray.add(CurrencyModel(value.getString("Id"),
                                value.getString("Url"),
                                value.getString("ImageUrl"),
                                value.getString("Name"),
                                value.getString("Symbol"),
                                value.getString("CoinName"),
                                value.getString("FullName"),
                                value.getString("Algorithm"),
                                value.getString("ProofType"),
                                value.getString("FullyPremined"),
                                value.getString("TotalCoinSupply"),
                                value.getString("SortOrder"),
                                ""))
                    } catch (e: Exception) {
                        currencyArray.add(CurrencyModel(value.getString("Id"),
                                value.getString("Url"),
                                "",
                                value.getString("Name"),
                                value.getString("Symbol"),
                                value.getString("CoinName"),
                                value.getString("FullName"),
                                value.getString("Algorithm"),
                                value.getString("ProofType"),
                                value.getString("FullyPremined"),
                                value.getString("TotalCoinSupply"),
                                value.getString("SortOrder"),
                                ""))
                    }
                }
            val dataArray: ArrayList<CurrencyModel> = ArrayList()
            Collections.sort(currencyArray, { currencyModel, currencyModelNext -> currencyModel.sortOrder.toInt() - currencyModelNext.sortOrder.toInt() })
            for (i in 0 until  Prefs.getInt("count_currency", 40) ) {
                dataArray.add(currencyArray[i])
            }
            return dataArray
        } catch (e: Exception){
            Log.e("Error", "transform", e)
        }
        return null
    }

}

fun getCurrencyExchange(indexCurrency: Int) : String =
        when (indexCurrency) {
            0 -> "USD"
            1 -> "EUR"
            2 -> "UAH"
            else -> ""
        }
