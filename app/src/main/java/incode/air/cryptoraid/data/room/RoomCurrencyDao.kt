package incode.air.cryptoraid.data.room

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import io.reactivex.Flowable

@Dao
interface RoomCurrencyDao {

    @Query(RoomContract.SELECT_CURRENCIES)
    fun getAllData(): Flowable<List<CurrencyEntity>>

}