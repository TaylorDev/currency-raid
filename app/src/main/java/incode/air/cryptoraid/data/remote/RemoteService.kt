package incode.air.cryptoraid.data.remote

import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface  RemoteService {

    @GET(RemoteContract.ALL_DATA)
    fun requestAllCurrencyList(): Observable<retrofit2.Response<ResponseBody>>

    @GET(RemoteContract.PRICE_DATA)
    fun requestAllPriceData(@Query(RemoteContract.FSYMS) fsyms: String,
                            @Query(RemoteContract.TSYMS) tsyms: String): Observable<Response<ResponseBody>>


    @GET(RemoteContract.HISTORICAL_DAILY)
    fun getHistoricalDaily(@Query(RemoteContract.FSYM) fsym: String,
                           @Query(RemoteContract.TSYM) tsym: String,
                           @Query(RemoteContract.LIMIT) limit: Int): Observable<Response<ResponseBody>>

    @GET(RemoteContract.HISTORICAL_HOURLY)
    fun getHistoricalHourly(@Query(RemoteContract.FSYM) fsym: String,
                           @Query(RemoteContract.TSYM) tsym: String,
                           @Query(RemoteContract.LIMIT) limit: Int): Observable<Response<ResponseBody>>

}