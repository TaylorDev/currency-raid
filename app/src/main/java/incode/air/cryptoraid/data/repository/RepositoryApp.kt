package incode.air.cryptoraid.data.repository

import android.arch.lifecycle.LiveData
import incode.air.cryptoraid.model.CurrencyModel
import incode.air.cryptoraid.model.HistoricalModel

interface RepositoryApp {

    fun getAllCurrencyData(): LiveData<MutableList<CurrencyModel>>

    fun getAllPriceData(value: MutableList<CurrencyModel>): LiveData<MutableList<CurrencyModel>>

    fun getHistoricalDaylyData(fsyms: String, tsyms: String, limit: Int): LiveData<HashMap<String, HistoricalModel>>

    fun getHistoricalHorlyData(fsyms: String, tsyms: String, limit: Int): LiveData<HashMap<String, HistoricalModel>>

}