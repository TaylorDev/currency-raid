package incode.air.cryptoraid.data.remote

import javax.inject.Inject

class RemoteDataSource @Inject constructor(private val remoteService: RemoteService) {

    fun requestAllCurrencyList() = remoteService.requestAllCurrencyList()

    fun requestAllPriceData(fsyms: String, tsyms: String) = remoteService.requestAllPriceData(fsyms, tsyms)

    fun requestHistoricalDaily(fsym: String, tsym: String, limit: Int) = remoteService.getHistoricalDaily(fsym, tsym, limit)

    fun requestHistoricalHourly(fsym: String, tsym: String, limit: Int) = remoteService.getHistoricalHourly(fsym, tsym, limit)

}