package incode.air.cryptoraid.di

import dagger.Component
import incode.air.cryptoraid.ui.currency_list.CurrencyListViewModel
import incode.air.cryptoraid.ui.detail_currency.DetailCurrencyViewModel
import incode.air.cryptoraid.ui.setting.SettingViewModel
import incode.air.cryptoraid.ui.splash.SplashViewModel
import javax.inject.Singleton

@Component(modules = [(AppModule::class), (RoomModule::class), (RemoteModule::class)])
@Singleton
interface AppComponent {

    fun inject(viewModel: SplashViewModel)

    fun inject(viewModel: CurrencyListViewModel)

    fun inject(viewModel: SettingViewModel)

    fun inject(viewModel: DetailCurrencyViewModel)

}