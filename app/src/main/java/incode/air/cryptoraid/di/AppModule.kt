package incode.air.cryptoraid.di

import android.content.Context
import dagger.Module
import dagger.Provides
import incode.air.cryptoraid.app.CryptoRaidApplication
import javax.inject.Singleton

@Module
class AppModule(private val cryptoRaidApplication: CryptoRaidApplication) {

    @Provides
    @Singleton
    fun provideContext(): Context = cryptoRaidApplication
}