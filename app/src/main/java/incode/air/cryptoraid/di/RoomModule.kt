package incode.air.cryptoraid.di

import android.content.Context
import dagger.Module
import dagger.Provides
import incode.air.cryptoraid.data.room.buildPersistentCurrency
import javax.inject.Singleton

@Module
class RoomModule {

    @Provides
    @Singleton
    fun provideRoomDataSource(context: Context) = buildPersistentCurrency(context)

}