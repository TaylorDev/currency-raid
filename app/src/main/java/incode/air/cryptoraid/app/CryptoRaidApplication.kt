package incode.air.cryptoraid.app

import android.app.Application
import incode.air.cryptoraid.di.*
import timber.log.Timber
import android.content.ContextWrapper
import com.pixplicity.easyprefs.library.Prefs



class CryptoRaidApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(packageName)
                .setUseDefaultSharedPreference(true)
                .build()
        appComponent =  DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .roomModule(RoomModule())
                .remoteModule(RemoteModule()).build()
    }

}

lateinit var appComponent: AppComponent


