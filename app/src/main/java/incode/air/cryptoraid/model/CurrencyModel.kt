package incode.air.cryptoraid.model

import java.io.Serializable

data class CurrencyModel (
        val id: String,
        val url: String,
        val imageUrl: String,
        val name: String,
        val symbol: String,
        val coinName: String,
        val fullName: String,
        val algorithm: String,
        val proofType: String,
        val fullyPremined: String,
        val totalCoinSupply: String,
        val sortOrder: String,
        var priceData: String
) : Serializable