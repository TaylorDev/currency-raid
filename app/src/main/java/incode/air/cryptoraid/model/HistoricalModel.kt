package incode.air.cryptoraid.model

data class HistoricalModel(
		val close: Double? = null,
		val high: Double? = null,
		val low: Double? = null,
		val open: Double? = null,
		val volumefrom: Double? = null,
		val volumeto: Double? = null
)
