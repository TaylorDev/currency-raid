package incode.air.cryptoraid.ui.currency_list

import android.arch.lifecycle.*
import incode.air.cryptoraid.app.appComponent
import incode.air.cryptoraid.model.CurrencyModel
import incode.air.cryptoraid.data.repository.Repository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class CurrencyListViewModel: ViewModel(), LifecycleObserver {

    @Inject
    lateinit var repository: Repository

    private val compositeDisposable = CompositeDisposable()
    private var listCurrency: LiveData<MutableList<CurrencyModel>>? = null

    init {
        appComponent.inject(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun unSubscribeViewModel() {
        for (disposable in repository.allCompositeDisposable) {
            compositeDisposable.addAll(disposable)
        }
        compositeDisposable.clear()
    }

    fun getListCurrency(): LiveData<MutableList<CurrencyModel>>? {
        listCurrency = null
        listCurrency = MutableLiveData<MutableList<CurrencyModel>>()
        listCurrency = repository.getAllCurrencyData()
        return listCurrency
    }

    fun getDataCurrency(data: MutableList<CurrencyModel>): LiveData<MutableList<CurrencyModel>>? {
        listCurrency = null
        listCurrency = MutableLiveData<MutableList<CurrencyModel>>()
        listCurrency = repository.getAllPriceData(data)
        return listCurrency
    }

    fun getAllCurrencyData(): LiveData<MutableList<CurrencyModel>>?{
        return listCurrency
    }

}

fun getCurrencySign(currency: String): String =  when(currency){
    "USD" -> "\u0024"
    "EUR" -> "\u20AC"
    "UAH" -> "\u20B4"
    else ->  ""
}