package incode.air.cryptoraid.ui.currency_list

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.pixplicity.easyprefs.library.Prefs
import incode.air.cryptoraid.R
import incode.air.cryptoraid.data.repository.getCurrencyExchange
import incode.air.cryptoraid.model.CurrencyModel
import java.util.*

class CurrencyListAdapter (var callback: CurrencyHolder.PositionCallback) : RecyclerView.Adapter<CurrencyHolder>() {

    private var data: MutableList<CurrencyModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyHolder {
        val inflatedView = LayoutInflater.from(parent.context)
                .inflate(R.layout.currency_list_item , parent , false)
        return CurrencyHolder(inflatedView, callback)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: CurrencyHolder, position: Int) {
        when {
            data[position].imageUrl != "" -> {
                val dataImg = "https://www.cryptocompare.com" + data[position].imageUrl
                Glide.with(holder.getContext()).load("https://www.cryptocompare.com" + data[position].imageUrl).into(holder.imageCurrecy)
            }
        }
        holder.nameCurrency.text = data[position].coinName
        holder.symbolCurrency.text = data[position].symbol
        holder.priceCurrency.text = getCurrencySign(getCurrencyExchange(Prefs.getInt("currency", 0))) +
                String.format("%.2f", data[position].priceData.toDouble())
    }



    override fun getItemCount(): Int {
        return data.size
    }

    fun setData(list: MutableList<CurrencyModel>?){
        data.clear()
        list?.toTypedArray()?.let { data.addAll(it) }
    }

}