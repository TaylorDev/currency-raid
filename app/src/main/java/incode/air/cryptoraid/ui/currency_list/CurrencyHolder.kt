package incode.air.cryptoraid.ui.currency_list

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import incode.air.cryptoraid.R

class CurrencyHolder internal constructor(itemView: View, callback: PositionCallback) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

    val imageCurrecy = itemView.findViewById<ImageView>(R.id.img_currency)
    val symbolCurrency = itemView.findViewById<TextView>(R.id.symbol_currency)
    val priceCurrency = itemView.findViewById<TextView>(R.id.price_currency)
    val nameCurrency = itemView.findViewById<TextView>(R.id.name_currency)
    var callback: PositionCallback

    init {
        itemView.setOnClickListener(this)
        this.callback = callback
    }

    fun getContext(): Context {
        return itemView.context
    }

    @SuppressLint("LogNotTimber")
    override fun onClick(v: View?) {
        callback.getPosition(position)
    }

    interface PositionCallback{

        fun getPosition(position: Int)

    }
}