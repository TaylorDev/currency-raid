package incode.air.cryptoraid.ui.detail_currency

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import com.bumptech.glide.Glide
import com.pixplicity.easyprefs.library.Prefs
import incode.air.cryptoraid.R
import incode.air.cryptoraid.data.repository.getCurrencyExchange
import incode.air.cryptoraid.model.CurrencyModel
import incode.air.cryptoraid.ui.currency_list.getCurrencySign
import incode.air.cryptoraid.ui.setting.getStartSettingActivity
import kotlinx.android.synthetic.main.fragment_detail_currency.*
import org.eazegraph.lib.models.ValueLinePoint
import org.eazegraph.lib.models.ValueLineSeries
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


open class DetailCurrencyFragment : Fragment(), ScaleGestureDetector.OnScaleGestureListener {
    private var detailCurrencyViewModel: DetailCurrencyViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        initViewModel()
    }

    override fun onScaleBegin(detector: ScaleGestureDetector?): Boolean {
        return false
    }

    override fun onScaleEnd(detector: ScaleGestureDetector?) {

    }

    override fun onScale(detector: ScaleGestureDetector?): Boolean {
        return false
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_detail_currency, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
    }

    private fun initViewModel() {
        detailCurrencyViewModel = ViewModelProviders.of(this).get(DetailCurrencyViewModel::class.java)
        detailCurrencyViewModel?.let { lifecycle.addObserver(it) }
    }

    @SuppressLint("SetTextI18n")
    private fun initUi() {
        val data:CurrencyModel = arguments?.getSerializable("data_currency") as CurrencyModel
        symbol_detail.text = data.symbol
        price_detail.text = getCurrencySign(getCurrencyExchange(Prefs.getInt("currency", 0))) + data.priceData
        when {
            data.imageUrl != "" ->
                context?.let { Glide.with(it).load("https://www.cryptocompare.com" + data.imageUrl).into(icon_detail) }
        }
        rank_data.text = data.sortOrder
        if(data.totalCoinSupply == "N/A"){
            available_supply_data.text = data.totalCoinSupply
        } else {
            available_supply_data.text = getCurrencySign(getCurrencyExchange(Prefs.getInt("currency", 0))) +
                    (data.totalCoinSupply.toFloat() / 1000000).toString() + "M"
        }
        day_type.setOnClickListener {
            setDefaultStateForeButton()
            day_type.setTextColor(Color.parseColor("#4842f4"))
            Prefs.putString("type", "day")
            populateHourlyListAdapter(data.symbol.toUpperCase(), 24)
        }

        week_type.setOnClickListener{
            setDefaultStateForeButton()
            week_type.setTextColor(Color.parseColor("#4842f4"))
            Prefs.putString("type", "week")
            populateDailyListAdapter(data.symbol.toUpperCase(), 7)
        }

        month_type.setOnClickListener {
            setDefaultStateForeButton()
            month_type.setTextColor(Color.parseColor("#4842f4"))
            Prefs.putString("type", "month")
            populateDailyListAdapter(data.symbol.toUpperCase(), 30)
        }

        year_type.setOnClickListener {
            setDefaultStateForeButton()
            year_type.setTextColor(Color.parseColor("#4842f4"))
            Prefs.putString("type", "year")
            populateDailyListAdapter(data.symbol.toUpperCase(), 360)
        }

        when (Prefs.getString("type", "year")){
            "day" -> {
                day_type.setTextColor(Color.parseColor("#4842f4"))
                populateHourlyListAdapter(data.symbol.toUpperCase(), 24)
            }
            "week" -> {
                week_type.setTextColor(Color.parseColor("#4842f4"))
                populateDailyListAdapter(data.symbol.toUpperCase(), 7)
            }
            "month" -> {
                month_type.setTextColor(Color.parseColor("#4842f4"))
                populateDailyListAdapter(data.symbol.toUpperCase(), 30)
            }
            "year" ->{
                year_type.setTextColor(Color.parseColor("#4842f4"))
                populateDailyListAdapter(data.symbol.toUpperCase(), 360)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.setting_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.action_setting_detail) {
            startActivity(context?.let { getStartSettingActivity(it) })
        }
        return super.onOptionsItemSelected(item)
    }

    fun setDefaultStateForeButton(){
        day_type.setTextColor(resources.getColor(R.color.black))
        week_type.setTextColor(resources.getColor(R.color.black))
        month_type.setTextColor(resources.getColor(R.color.black))
        year_type.setTextColor(resources.getColor(R.color.black))
    }

    @SuppressLint("SimpleDateFormat")
    private fun populateDailyListAdapter(currency: String, limit: Int) {
        detailCurrencyViewModel?.getHistoricalData(currency, "USD", limit)?.observe(this, android.arch.lifecycle.Observer { historical ->
            val series = ValueLineSeries()
            series.color = Color.parseColor("#4842f4")
            clear()
            val tempArray = ArrayList<Float>()
            cubiclinechart.isScrollContainer = false
            cubiclinechart.clearStandardValues()
            val keys = historical?.keys?.toList()
            Collections.sort(keys) { o1, o2 ->
                val i1 = Integer.parseInt(o1)
                val i2 = Integer.parseInt(o2)
                if (i1 < i2) -1 else if (i1 === i2) 0 else 1
            }

            keys?.forEach {
                val date = Date(it.toLong() * 1000)
                val sdf = SimpleDateFormat("dd MMM")
                val formattedDate = sdf.format(date)
                historical[it]?.high?.toFloat()?.let { it1 -> tempArray.add(it1) }
                historical[it]?.high?.toFloat()?.let { it1 -> series.addPoint(ValueLinePoint(formattedDate, it1)) }
            }

            getPercentRates(tempArray)

            val z = minValue - maxValue
            val n = z / maxValue
            val percent = n * 100

            if(percent.toString().contains("-")){
                rates_data.setImageResource(R.drawable.ic_trending_down_black_24dp)
                rates_value.setTextColor(Color.parseColor("#FF4040"))
                rates_value.text = String.format("%(.2f", percent.toString().substring(1, percent.toString().length).toFloat()) + "%"
            } else {
                rates_data.setImageResource(R.drawable.ic_trending_up_black_24dp)
                rates_value.setTextColor(Color.parseColor("#0CB038"))
                rates_value.text = String.format("%(.2f", percent.toString().substring(1, percent.toString().length).toFloat()) + "%"
            }
            Timber.d("Currency Rates %s", percent.toString())

            cubiclinechart.addSeries(series)
            cubiclinechart.indicatorLineColor
            cubiclinechart.startAnimation()
        })
    }

    var maxValue = 0.0f
    var minValue = 0.0f

    @SuppressLint("SimpleDateFormat")
    private fun populateHourlyListAdapter(currency: String, limit: Int) {
        detailCurrencyViewModel?.getHourlyData(currency, "USD", limit)?.observe(this, android.arch.lifecycle.Observer { historical ->
            val series = ValueLineSeries()
            series.color = -0x4842f4
            clear()
            val keys = historical?.keys?.toList()
            val tempArray = ArrayList<Float>()
            Collections.sort(keys) { o1, o2 ->
                val i1 = Integer.parseInt(o1)
                val i2 = Integer.parseInt(o2)
                if (i1 < i2) -1 else if (i1 === i2) 0 else 1
            }

            keys?.forEach {
                val date = Date(it.toLong() * 1000)
                val sdf = SimpleDateFormat("HH:mm")
                val formattedDate = sdf.format(date)
                historical[it]?.high?.toFloat()?.let { it1 -> tempArray.add(it1) }
                historical[it]?.high?.toFloat()?.let { it1 -> series.addPoint(ValueLinePoint(formattedDate, it1)) }
            }

            getPercentRates(tempArray)

            val z = minValue - maxValue
            val n = z / maxValue
            val percent = n * 100

            Timber.d("Currency Rates %s", percent.toString())

            cubiclinechart.addSeries(series)
            cubiclinechart.resetZoom(false)
            cubiclinechart.indicatorLineColor
            cubiclinechart.startAnimation()
        })
    }

    private fun getPercentRates(tempArray: ArrayList<Float>) {
        minValue = tempArray[tempArray.size - 1]
        maxValue = tempArray[0]
    }

    fun clear(){
        cubiclinechart.clearChart()
    }

}

fun newInstanceDetailCurrency() = DetailCurrencyFragment()