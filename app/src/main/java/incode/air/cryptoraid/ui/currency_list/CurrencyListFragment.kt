package incode.air.cryptoraid.ui.currency_list

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import incode.air.cryptoraid.R
import kotlinx.android.synthetic.main.fragment_currency_list.*
import android.support.v4.view.MenuItemCompat
import android.support.v4.view.MenuItemCompat.getActionView
import android.support.v7.widget.SearchView
import android.view.*
import incode.air.cryptoraid.model.CurrencyModel
import incode.air.cryptoraid.ui.detail_currency.getStartDetailCurrencyActivity
import incode.air.cryptoraid.ui.setting.getStartSettingActivity
import incode.air.cryptoraid.ui.setting.updateData
import kotlin.collections.ArrayList


class CurrencyListFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener, CurrencyHolder.PositionCallback {

    private var currencyListViewModel: CurrencyListViewModel? = null
    private lateinit var currenciesAdapter: CurrencyListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        initViewModel()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_currency_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        populateCurrencyListAdapter()
    }

    override fun onResume() {
        if(updateData) {
            swipeRefreshState(true)
            populateCurrencyListAdapter()
        }
        super.onResume()
    }

    private fun initUi() {
        refresh_list_currency.setOnRefreshListener(this)
        swipeRefreshState(true)
        initRecycler()
    }

    private fun swipeRefreshState(state: Boolean){
        refresh_list_currency.isRefreshing = state
    }

    private fun initRecycler() {
        currenciesAdapter = CurrencyListAdapter(this)
        recycler_list_currency.layoutManager = LinearLayoutManager(context)
        recycler_list_currency.adapter = currenciesAdapter
    }

    private fun populateCurrencyListAdapter() {
        currencyListViewModel?.getListCurrency()?.observe(this@CurrencyListFragment, Observer { currencyList ->
            currencyList.let {
                currencyList?.let { list ->
                    currencyListViewModel?.getDataCurrency(list)?.observe(this@CurrencyListFragment, Observer { currencyList ->
                            currenciesAdapter.setData(currencyList)
                            currenciesAdapter.notifyDataSetChanged()
                            swipeRefreshState(false)
                        })
                    }
                }
            })
    }

    private fun initViewModel() {
        currencyListViewModel = ViewModelProviders.of(this).get(CurrencyListViewModel::class.java)
        currencyListViewModel?.let { lifecycle.addObserver(it) }
        currencyListViewModel?.getListCurrency()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.search_menu, menu)
        val item = menu.findItem(R.id.action_search)
        val searchView = getActionView(item) as SearchView
        MenuItemCompat.setShowAsAction(item, MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW or MenuItem.SHOW_AS_ACTION_IF_ROOM)
        MenuItemCompat.setActionView(item, searchView)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                val listDataFinding: ArrayList<CurrencyModel> = ArrayList()
                currencyListViewModel?.getAllCurrencyData()?.observe( this@CurrencyListFragment, Observer {currencyList ->
                    currencyList?.forEach {
                        when {
                            it.coinName.toLowerCase().contains(newText.toLowerCase()) -> listDataFinding.add(it)
                            it.symbol.toLowerCase().contains(newText.toLowerCase()) -> listDataFinding.add(it)
                        }
                    }
                    currenciesAdapter.setData(listDataFinding)
                    currenciesAdapter.notifyDataSetChanged()
                })
                return true
            }
        })

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.action_setting) {
            startActivity(context?.let { getStartSettingActivity(it) })
        }
        return super.onOptionsItemSelected(item)
    }

    override fun getPosition(position: Int) {
        val intent = context?.let { getStartDetailCurrencyActivity(it)}
        intent?.putExtra("data_currency", currencyListViewModel?.getAllCurrencyData()?.value?.get(position))
        startActivity(context?.let { intent })
    }

    override fun onRefresh() {
        swipeRefreshState(true)
        populateCurrencyListAdapter()
    }
}

fun newInstanceCurrencyList() = CurrencyListFragment()