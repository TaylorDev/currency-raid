package incode.air.cryptoraid.ui.splash

import android.arch.lifecycle.*
import incode.air.cryptoraid.app.appComponent
import incode.air.cryptoraid.model.CurrencyModel
import incode.air.cryptoraid.data.repository.Repository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SplashViewModel: ViewModel(), LifecycleObserver {

    @Inject
    lateinit var repository: Repository

    private val compositeDisposable = CompositeDisposable()
    private var listCurrency: LiveData<ArrayList<CurrencyModel>>? = null

    init {
        appComponent.inject(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun unSubscribeViewModel() {
        for (disposable in repository.allCompositeDisposable) {
            compositeDisposable.addAll(disposable)
        }
        compositeDisposable.clear()
    }

}