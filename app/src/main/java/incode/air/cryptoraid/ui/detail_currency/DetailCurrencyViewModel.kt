package incode.air.cryptoraid.ui.detail_currency

import android.arch.lifecycle.*
import incode.air.cryptoraid.app.appComponent
import incode.air.cryptoraid.data.repository.Repository
import incode.air.cryptoraid.model.CurrencyModel
import incode.air.cryptoraid.model.HistoricalModel
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class DetailCurrencyViewModel: ViewModel(), LifecycleObserver {

    @Inject
    lateinit var repository: Repository

    private val compositeDisposable = CompositeDisposable()

    private var listCurrency: LiveData<HashMap<String, HistoricalModel>>? = null

    init {
        appComponent.inject(this)
    }

    fun getHistoricalData(currency: String, currencyExchange: String, limit: Int): LiveData<HashMap<String, HistoricalModel>>? {
        listCurrency = null
        listCurrency = MutableLiveData<HashMap<String, HistoricalModel>>()
        listCurrency = repository.getHistoricalDaylyData(currency, currencyExchange, limit)
        return listCurrency
    }

    fun getHourlyData(currency: String, currencyExchange: String, limit: Int): LiveData<HashMap<String, HistoricalModel>>? {
        listCurrency = null
        listCurrency = MutableLiveData<HashMap<String, HistoricalModel>>()
        listCurrency = repository.getHistoricalHorlyData(currency, currencyExchange, limit)
        return listCurrency
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun unSubscribeViewModel() {
        for (disposable in repository.allCompositeDisposable) {
            compositeDisposable.addAll(disposable)
        }
        compositeDisposable.clear()
    }

}