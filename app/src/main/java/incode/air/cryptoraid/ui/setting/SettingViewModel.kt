package incode.air.cryptoraid.ui.setting

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.arch.lifecycle.ViewModel
import incode.air.cryptoraid.app.appComponent
import incode.air.cryptoraid.data.repository.Repository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SettingViewModel: ViewModel(), LifecycleObserver {

    @Inject
    lateinit var repository: Repository

    private val compositeDisposable = CompositeDisposable()

    init {
        appComponent.inject(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun unSubscribeViewModel() {
        for (disposable in repository.allCompositeDisposable) {
            compositeDisposable.addAll(disposable)
        }
        compositeDisposable.clear()
    }

}