package incode.air.cryptoraid.ui.currency_list

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import incode.air.cryptoraid.R

class CurrencyListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_currency_list)
        if (savedInstanceState == null) {
            replaceFragment(newInstanceCurrencyList())
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.content, fragment)
                .commit()
    }

}

fun getStartCurrencyListActivity(context: Context): Intent {
    return Intent(context, CurrencyListActivity::class.java)
}
