package incode.air.cryptoraid.ui.detail_currency

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.MenuItem
import incode.air.cryptoraid.R
import timber.log.Timber

class DetailCurrencyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_currency)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val fragment = newInstanceDetailCurrency()
        val bundle = Bundle()
        bundle.putSerializable("data_currency", intent.getSerializableExtra("data_currency"))
        fragment.arguments = bundle
        if (savedInstanceState == null) {
            replaceFragment(fragment)
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.content_detail, fragment)
                .commit()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when {
            item.itemId == android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

}

fun getStartDetailCurrencyActivity(context: Context): Intent {
    return Intent(context, DetailCurrencyActivity::class.java)
}
