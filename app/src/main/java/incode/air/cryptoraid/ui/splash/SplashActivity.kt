package incode.air.cryptoraid.ui.splash

import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import incode.air.cryptoraid.R
import incode.air.cryptoraid.ui.currency_list.getStartCurrencyListActivity

class SplashActivity : AppCompatActivity() {

    private var splashViewModel: SplashViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        initViewModel()
        startActivity(getStartCurrencyListActivity(this))
    }

    private fun initViewModel() {
        splashViewModel = ViewModelProviders.of(this).get(SplashViewModel::class.java)
        splashViewModel?.let { lifecycle.addObserver(it) }
    }

}
