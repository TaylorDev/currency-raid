package incode.air.cryptoraid.ui.setting

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import com.pixplicity.easyprefs.library.Prefs
import com.xw.repo.BubbleSeekBar
import incode.air.cryptoraid.R
import kotlinx.android.synthetic.main.fragment_setting.*

class SettingFragment : Fragment(), SeekBar.OnSeekBarChangeListener {

    private var settingViewModel: SettingViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_setting, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
    }

    private fun initUi() {
        spinner_currency.setSelection(Prefs.getInt("currency", 0))
        currency_count.text = Prefs.getInt("count_currency", 40).toString()
        seek_count.progress = Prefs.getInt("count_currency", 40)
        seek_count.setOnSeekBarChangeListener(this)
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        currency_count.text = progress.toString()
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
        currency_count.text = seekBar?.progress.toString()
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        currency_count.text = seekBar?.progress.toString()
    }

    private fun initViewModel() {
        settingViewModel = ViewModelProviders.of(this).get(SettingViewModel::class.java)
        settingViewModel?.let { lifecycle.addObserver(it) }
    }

    override fun onPause() {
        updateData = if(Prefs.getInt("currency", 0) != spinner_currency.selectedItemPosition ||
                Prefs.getInt("count_currency", 40) != seek_count.progress){
            Prefs.putInt("currency", spinner_currency.selectedItemPosition)
            Prefs.putInt("count_currency", seek_count.progress)
            true
        } else {
            false
        }
        super.onPause()
    }

}

var updateData: Boolean = false

fun newInstanceSetting() = SettingFragment()